"""
geocode reverser : location name to coordinates
"""

__authors__ = "Raphael Cere"

import urllib2
import json

def georeverser(l, k):
	r = urllib2.urlopen("https://maps.googleapis.com/maps/api/geocode/json?address="+l+"&key="+k).read()
	return r

def savejson(j, l):
	c = open(l+".json", 'w')
	c.write(str(j))

if __name__ == "__main__":
	l = raw_input("LOCATION NAME: ")
	k = raw_input("YOUR API KEY: ")
	r = georeverser(l, k)
	savejson(r, l)